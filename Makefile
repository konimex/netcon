export VERSION = 1.29

PKG_CONFIG ?= pkg-config

SHELL=/bin/bash

sd_var = $(shell $(PKG_CONFIG) --variable=systemd$(1) systemd)
systemdsystemconfdir = $(call sd_var,systemconfdir)
systemdsystemunitdir = $(call sd_var,systemunitdir)

.PHONY: install tarball pkgbuild upload clean

install:
	# Documentation
	$(MAKE) -C docs install
	# Configuration files
	install -d $(DESTDIR)/etc/netcon/{examples,hooks,interfaces}
	install -m644 docs/examples/* $(DESTDIR)/etc/netcon/examples/
	# Libs
	install -Dt $(DESTDIR)/usr/lib/netcon -m644 src/lib/{globals,interface,ip,rfkill,wpa}
	install -Dt $(DESTDIR)/usr/lib/netcon/connections -m644 src/lib/connections/*
	install -Dt $(DESTDIR)/usr/lib/netcon/dhcp -m644 src/lib/dhcp/*
	install -m755 src/lib/{auto.action,network} $(DESTDIR)/usr/lib/netcon/
	# Scripts
	install -d $(DESTDIR)/usr/bin
	sed -e "s|@systemdsystemconfdir@|$(systemdsystemconfdir)|g" \
	    -e "s|@systemdsystemunitdir@|$(systemdsystemunitdir)|g" \
	    src/netcon.in > $(DESTDIR)/usr/bin/netcon
	chmod 755 $(DESTDIR)/usr/bin/netcon
	install -m755 \
	    src/netcon-auto \
	    src/wifi-menu \
	    $(DESTDIR)/usr/bin/
	install -Dm755 src/ifplugd.action $(DESTDIR)/etc/ifplugd/netcon.action
	# Services
	install -Dt $(DESTDIR)$(systemdsystemunitdir) -m644 services/*.service

tarball: netcon-$(VERSION).tar.xz
netcon-$(VERSION).tar.xz:
	$(MAKE) -B -C docs
	cp src/lib/globals{,.orig}
	sed -i "s|NETCON_VERSION=.*|NETCON_VERSION=$(VERSION)|" src/lib/globals
	git stash save -q
	git archive -o netcon-$(VERSION).tar --prefix=netcon-$(VERSION)/ stash
	git stash pop -q
	mv src/lib/globals{.orig,}
	tar --exclude-vcs --transform "s|^|netcon-$(VERSION)/|" --owner=root --group=root --mtime=./netcon-$(VERSION).tar -rf netcon-$(VERSION).tar docs/*.[1-8]
	xz netcon-$(VERSION).tar
	gpg --detach-sign $@

pkgbuild: PKGBUILD
PKGBUILD: netcon-$(VERSION).tar.xz netcon.install contrib/PKGBUILD.in
	sed -e "s|@pkgver@|$(VERSION)|g" \
	    -e "s|@sha256sum@|$(shell sha256sum $< | cut -d ' ' -f 1)|" \
	    $(lastword $^) > $@

netcon.install: contrib/netcon.install
	cp $< $@

upload: netcon-$(VERSION).tar.xz
	scp $< $<.sig sources.archlinux.org:/srv/ftp/other/packages/netcon

clean:
	$(MAKE) -C docs clean
	-@rm -vf netcon-*.tar.xz{,.sig} PKGBUILD netcon.install
